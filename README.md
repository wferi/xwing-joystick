# Joystick support

The original X-Wing game (1993) handles three buttons and two axes
only.  To make use of the other features, I set up these bindings
in the Dosbox keymapper for a Thrustmaster T-Flight Stick X:

## Buttons

|  # | binding | action |
| -- | ------- | ------ |
|  1 | default | fire
|  2 | default | roll/target ship in sight
|  3 | w [^1]  | change weapon
|  4 | default | toggle cockpit (.)
|  5 | s       | balance shields
|  6 | r       | target nearest figher
|  7 | F10     | increase shield recharge
|  8 | F9      | increase laser recharge
|  9 | x       | cycle fire mode
| 10 | SPACE   | ack R2 message
| 11 | m       | map
| 12 | i       | toggle computer mode

[^1]: Button 3 is used internally as u (target ship in sight),
which is also on button 2, so delete its default assignment
from the emulated joystick.

## Hat

To be able to actually use the functions bound to the hat switch,
`joysticktype` must be set to `fcs` or `ch`.  (This is an obscure
limitation of Dosbox 0.74, which can be patched out [^2].)  The former
has only three axes, so if you want to use a fourth axis for throttle
(see [below](#generally) for an example), use the latter.

[^2]: https://www.vogons.org/viewtopic.php?f=32&t=27452&p=314840&hilit=joystick#p314840

X-Wing assigns rather arbitrary effects to the hat switch, so make
sure to delete the default bindings before assigning your custom
effects.  I chose these:

| direction | binding | action |
| --------- | ------- | ------ |
| left      | '       | transfer energy to shields
| right     | ;       | transfer energy to lasers
| up        | y       | next target
| down      | t       | previous target

## Throttle

### On Linux

The `throttle` Python script translates throttle events into key
presses interpreted by X-Wing.  Synchronization is needed at the start
of each mission by momentarily passing through zero or full throttle
(the only points of absolute control).

Its dependencies are (on a Debian system):

* python3-libevdev
* python3-xlib

### Generally

If you run `joy.com`[^3] before starting the game (in Dosbox), it will
translate the position of your fourth joystick axis into the
appropriate number of =/- key presses to mirror the physical axis
position.  Synchronization happens at the end positions via sending
backslash or backspace.  Consider that reading joystick axis values is
a slow operation, so this approach might impact performance.

[^3]: `joy.com` was checked into the repository for convenience, you
can rebuild it from `joy.asm` if you need any customization.

---
