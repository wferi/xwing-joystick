;; Code based on joy.com found via
;; https://www.gog.com/forum/star_wars_xwing_and_tie_fighter_series/how_to_add_joystick_throttle_support_to_the_dos_version_joycom/post1
;; https://www.vogons.org/viewtopic.php?t=28807
;; https://www.vogons.org/download/file.php?id=9250
;;
;; Assemble by: nasm -Wall -o joy.com joy.asm

axis:	equ	3		; 0..3 for the four axes
button:	equ	0		; 0..3 for the four buttons

full:	equ	32		; full throttle level in-game

org 100h
start:
	jmp	init

;;; calibration finishes at full throttle:
band:	db	full		; 0..full (as selectable by +/- in game)
max:	dw	0		; maximum axis value (zero throttle)
range:	dw	0		; maximum-minimum axis value (full throttle)

;;; From https://www.fountainware.com/EXPL/bios_key_codes.htm:
backspace:	equ	0e08h
backslash:	equ	2c5ch
minus:		equ	0c2dh
equal:		equ	0d3dh

get_axis:			; returns value in CX
	mov	dx,201h
	xor	cx,cx
	out	dx,al		; start axis measurement (value arbitrary)
.read:
	in	al,dx
	test	al,1 << axis
	loopnz	.read		; count cycles until ready
	neg	cx		; loopnz decremented, we need increments
	ret

;;; returns band (0..full) in AX and axis value in CX
;;; band = round((max - value) * full / range) =
;;;      = floor(((max - value) * full + range / 2) / range)
throttle_band:
	call	get_axis
	mov	ax,[cs:max]
	sub	ax,cx		; max - value
	jl	.toobig		; value above maximum
	mov	cx,full
	mov	bx,[cs:range]
	cmp	ax,bx
	jg	.toosmall	; value below minimum
	mul	cx		; * full
	shr	bx,1
	add	ax,bx
	adc	dx,0		; + floor(range / 2)
	div	word [cs:range]	; / range
	ret
.toosmall:
	mov	ax,cx		; full throttle
	ret
.toobig:
	xor	ax,ax		; zero throttle
	ret

translate:	 ; called periodically by the hardware timer interrupt
	push	ax
	push	bx
	push	cx
	push	dx
	pushf
	call	throttle_band
	mov	bl,[cs:band]
	cmp	al,bl	; changed?
	je	.done	; no, chain forward
	or	al,al	; zero throttle?
	jz	.zero
	cmp	al,full ; full throttle?
	je	.full
	cmp	al,bl	; increase or decrease needed?
	ja	.increase
	mov	cx,minus
	dec	bl
	jmp	.send
.increase:
	mov	cx,equal
	inc	bl
	jmp	.send
.full:
	mov	cx,backspace
	mov	bl,full
	jmp	.send
.zero:
	mov	cx,backslash
	xor	bl,bl
.send:
	mov	ah,5	; int 16h keyboard buffer write
	int	16h	; store into keyboard buffer
	or	al,al
	jne	.done	; exit if failed
	mov	[cs:band],bl	; update current band
.done:
	popf
	pop	dx
	pop	cx
	pop	bx
	pop	ax
	jmp	0:0		; target patched in by init code
orig_vector:	equ	$-4

init:
	mov	ah,9		; print "down" message (zero throttle)
	mov	dx,down_msg
	int	21h
	call	get_axis_until_button_press
	mov	[max],si	; save it
	mov	[range],si	; and prepare computing the range
	mov	ah,9
	mov	dx,up_msg	; print "up" message (full throttle)
	int	21h
	call	wait_button_release
	call	get_axis_until_button_press
	sub	[range],si	; that's the range
	mov	ax,3508h
	int	21h		; get timer interrupt vector
	mov	[orig_vector],bx ; save it
	mov	[orig_vector+2],es
	mov	dx,translate
	mov	ax,2508h
	int	21h		; install our timer interrupt code
	mov	ax,[2ch]
	mov	es,ax
	mov	ah,49h
	int	21h		; free our environment segment
	mov	dx,(init-start+100h+15)/16
	mov	ax,3100h
	int	21h		; keep the active part resident

;;; Print 2Y axis value and finally return it in SI
;;; Returns the original value of SI if no measurement completed
get_axis_until_button_press:
.measure:
	mov	dx,201h
	xor	cx,cx
	out	dx,al		; start axis measurement (value arbitrary)
.read:
	in	al,dx
	test	al,1 << (button+4) ; pressed?
	jz	.done		; yes, return
	test	al,1 << axis	; measurement complete?
	loopnz	.read		; no, read again
	neg	cx		; (loopnz decremented, we want increments)
	mov	si,cx		; yes, store it...
	mov	dh,al
	call	print8bin	; and after the status byte
	mov	ax,si		; print it
	call	print16dec
	mov	dx,whiteout	; clean up after the number
	mov	ah,09h
	int	21h
	jmp	.measure
.done:
	ret

wait_button_release:
	mov	dx,201h
.read:
	in	al,dx
	test	al,1 << (button+4)
	jz	.read
	ret

;; https://stackoverflow.com/questions/45904075/displaying-numbers-with-dos
;; Print 16-bit decimal number in AX
print16dec:
	mov	bx,10
	xor	cx,cx		; reset digit counter
.compute:
	xor	dx,dx		; Setup for division DX:AX / BX
	div	bx		; -> AX is Quotient, Remainder DX=[0,9]
	push	dx		; (1) Save remainder for now
	inc	cx		; One more digit
	test	ax,ax		; Is quotient zero?
	jnz	.compute	; No, use as next dividend
.print:
	pop	dx		; (1)
	add	dl,"0"		; Turn into character [0,9] -> ["0","9"]
	mov	ah,02h		; DOS.DisplayCharacter
	int	21h		; -> AL
	loop	.print
	ret

;;; Print binary byte in DH and a space
print8bin:
	mov	cx,8
	mov	ah,2
.digit:
	mov	dl,'0'
	shl	dh,1
	jnc	.print
	inc	dl
.print:
	int	21h
	loop	.digit
	mov	dl,' '
	int	21h
	ret

down_msg:
	db	`Move dial to down position, and press button.\r\n$`
up_msg:
	db	`Move dial to up position, and press button.\r\n$`
whiteout:
	db	`\t\r$`
